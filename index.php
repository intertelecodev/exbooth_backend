<?php

/**
 * @file
 * The PHP page that serves all page requests on a Drupal installation.
 *
 * The routines here dispatch control to the appropriate handler, which then
 * prints the appropriate page.
 *
 * All Drupal code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 */

/**
 * Root directory of Drupal installation.
 */
$tmp_uri = $_SERVER['REQUEST_URI'];
if($tmp_uri=='/')exit;

//file_put_contents('log.txt','
//	'.date('Y-m-d H:i:s').' '.$tmp_uri.' # '.serialize($_REQUEST),FILE_APPEND);
define('DRUPAL_ROOT', getcwd());

require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
menu_execute_active_handler();
