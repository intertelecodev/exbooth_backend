<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php 
$time_elapsed = time() - $row->{$field->field_alias};
$seconds 	= $time_elapsed ;
$minutes 	= floor($time_elapsed / 60 );
$hours 		= floor($time_elapsed / 3600);
$days 		= floor($time_elapsed / 86400 );
$weeks 		= floor($time_elapsed / 604800);
//$months 	= floor($time_elapsed / 2600640 );
$years 		= floor($time_elapsed / 31207680 );

if($years)echo $years.'y';
elseif($weeks)echo $weeks.'w';
elseif($days)echo $days.'d';
elseif($hours)echo $hours.'h';
elseif($minutes)echo $minutes.'m';
elseif($seconds)echo $seconds.'s'; 
?>