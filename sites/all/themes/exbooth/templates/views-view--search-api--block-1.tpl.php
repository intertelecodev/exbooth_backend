<?php

  /**
   * @file
   * Main view template.
   *
   * Variables available:
   * - $classes_array: An array of classes determined in
   *   template_preprocess_views_view(). Default classes are:
   *     .view
   *     .view-[css_name]
   *     .view-id-[view_name]
   *     .view-display-id-[display_name]
   *     .view-dom-id-[dom_id]
   * - $classes: A string version of $classes_array for use in the class attribute
   * - $css_name: A css-safe version of the view name.
   * - $css_class: The user-specified classes names, if any
   * - $header: The view header
   * - $footer: The view footer
   * - $rows: The results of the view query, if any
   * - $empty: The empty text to display if the view is empty
   * - $pager: The pager next/prev links to display, if any
   * - $exposed: Exposed widget form/info to display
   * - $feed_icon: Feed icon to display, if any
   * - $more: A link to view more, if any
   *
   * @ingroup views_templates
   */
  /**
   * we have problem with services display so we create our tpl file 
   */
  //$view_result_strip_lines = preg_replace('/\s+/', '', strip_tags($rows));
  $data = substr(trim(strip_tags($rows)), 0, -1);

  /*$rows = NULL;
  $arr = json_decode('[' . $data . ']');

  $result = array();
  foreach ($arr as $key => $value) {

      $images = (array) $value->images[0];
      $productImageUrls = array();

      foreach ($images as $image) {
          $file = file_load($image); // handle multiple images in the future
          if (!is_object($file)) {
              continue;;
          }
          $productImageUrls[] = array(
              'baseImage' => file_create_url($file->uri),
              'thumbnail' => image_style_url('apithumbnail', $file->uri)
          );
      }

      unset($value->images);
      $value->productImageUrls = $productImageUrls;
      $result[] = $value;
  }

//  watchdog('views_rendered_field_batrick', json_encode($result));
  print json_encode($result);*/
  $results = '[' . $data . ']';
  $results_decoded = json_decode($results);
  if(count($results_decoded)){
    $current_page = $GLOBALS['pager_page_array'][0];
    $items_per_page = $GLOBALS['pager_limits'][0];
    $start = ($current_page) * $items_per_page + 1;
    $end = $start+count($results_decoded)-1; 

    $output = array(
      'results' => $results_decoded,
      'metadata' => array(
        'current_page' => $current_page,
        'total_pages' => $GLOBALS['pager_total'][0],
        'items_per_page' => $items_per_page,
        'display_start' => $start,
        'display_end' => $end,
        'total_results' => $GLOBALS['pager_total_items'][0],
      )
    );
    
  }
  else
    $output = array(
      'results' => array(),
      'metadata' => array());
  echo json_encode($output);
?>