<?php

/*
 * Here we define all generic code.
 */

/**
 * delete files attached to entities when update
 */
function api_delete_files($entityId, $entityType) {
  $entity = entity_load($entityType, array($entityId));

  switch ($entityType) {
    case 'commerce_product':
      $ret = array();
      foreach ($entity[$entityId]->field_images['und'] as $file) {

        $file = file_load($file['fid']);
        $file_usage = file_usage_list($file);

        if (isset($file_usage['file'][$entityType][$entityId]) && count($file_usage['file']) == 1 && count($file_usage['file'][$entityType]) == 1 && count($file_usage['file'][$entityType][$entityId])) {

          file_usage_delete($file, $entityType, NULL, $entityId, 0);
          file_delete($file, TRUE);
        }
      }
      break;
  }
}

/**
 * loggin user in code
 */
function apiLogin($name, $pass) {
  // get the access token for app authentication
  $options = array(
    'method' => 'POST',
    'data' => json_encode(array('grant_type' => 'client_credentials', 'client_id' => 'app-id', 'client_secret' => 'app-secret')),
    'headers' => array('Content-Type' => 'application/json'),
  );

  $result = drupal_http_request(url('oauth2/token', array('absolute' => TRUE)), $options);
  $token = json_decode($result->data);

  $options = array(
    'method' => 'POST',
    'data' => json_encode(array('username' => $name, 'password' => $pass)),
    'headers' => array('Content-Type' => 'application/json',
      'Authorization' => $token->data->token_type . ' ' . $token->data->access_token),
  );

  $res = drupal_http_request(url('api/user/login', array('absolute' => TRUE)), $options);
  echo $res->data;
  exit;
}

function upload_img($img, $file_name, $dir = NULL) {
  if (empty($img) || empty($file_name))
    return false;

  if (strpos($img, 'http') === 0)
    $data = file_get_contents($img);
  else
    $data = base64_decode(trim($img));

  $finfo = finfo_open();
  $mime_type = finfo_buffer($finfo, $data, FILEINFO_MIME_TYPE);
  finfo_close($finfo);
  // get file type png. jpg
  $ext = $mime_type ? str_replace('image/', '', $mime_type) : 'png';

  $path = 'public://' . $dir . $file_name . $ext;
  $file = file_save_data($data, $path, FILE_EXISTS_REPLACE);
  return $file;
}

/**
 * update user profile picture fid.
 * @param type $user_id
 * @param type $file_id
 * @return boolean
 */
function change_user_pic($user_id = FALSE, $file_id = FALSE) {
  if ($user_id && $file_id){
    return db_update('users')
      ->fields(array('picture' => $file_id))
      ->condition('uid', $user_id)
      ->execute();
  }
  
  return false;
}
