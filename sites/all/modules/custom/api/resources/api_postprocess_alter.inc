<?php

/*
 * Post process hook Implementation
 */

/**
 * Implements hook_postprocess_alter().
 * @param type $controller
 * @param type $args
 * @param type $result
 */
function api_postprocess_alter($controller, $args, $result) {
  $success = TRUE;
  switch ($controller['callback']) {

    case '_user_resource_retrieve':
      if (isset($result->uid)) {
        $data = $result;
        
        // get posts count
        $query = db_select('commerce_product', 'n');
        $query->condition('uid', $result->uid, '=');
        $query->condition('status', '1', '=');
        $query->addExpression('COUNT(1)', 'count');
        $tmp_result = $query->execute();

        if ($record = $tmp_result->fetchAssoc())
          $data->posts_count=$record['count'];
        else
          $data->posts_count = '';
        
        // get followes & following counter
        $flag_obj = $flag_obj2 = $user_obj = new stdClass();
        $flag_obj->name = 'follow_user';
        $flag_obj->fid = 8;
        $user_obj->uid = $result->uid;
        $data->following_count = flag_get_user_flag_counts($flag_obj,$user_obj);
        $followers_arr = flag_get_counts('user',$result->uid);
        if(isset($followers_arr['follow_user']))
          $data->followers_count = $followers_arr['follow_user'];
        else
          $data->followers_count = '0';
        
        $data->is_followed = $data->is_following = '0';
        global $user;
        if(!empty($user->uid))
        {
          if($result->uid != $user->uid)
          {
            module_load_include('inc', 'flag_service', 'flag_service');
            $flag_arr['flag_name'] = 'follow_user';
            $flag_arr['uid'] = $user->uid;
            $flag_arr['entity_id'] = $result->uid;
            if(flag_service_is_flagged($flag_arr))
              $data->is_followed = '1';
            
            $flag_arr['flag_name'] = 'follow_user';
            $flag_arr['uid'] = $result->uid;
            $flag_arr['entity_id'] = $user->uid;
            if(flag_service_is_flagged($flag_arr))
              $data->is_following = '1';
          }
        }
        
        $result = new stdClass();
        $result->data = $data;
      }
      break;

    case '_user_resource_login':
      if (isset($result->user)) {
        $success = TRUE;
        $result->data = $result->user;
        $result->data->sessionName = $result->session_name;
        $result->data->sessId = $result->sessid;

        $view = views_get_view('followed_cats');
        $result->data->followed_cats = $view->render('services_1');

        unset($result->sessid);
        unset($result->session_name);
        unset($result->user);
        unset($result->token);
      }
      break;

    case '_user_resource_create':
      if (isset($result['uid'])) {
        //instagram user account data
        if (isset($args[0]['instagram']) && !empty($args[0]['instagram'])) {
          $args[0]['instagram']['uid'] = $result['uid'];

          $args[0]['instagram']['username'] = $args[0]['name'];
          if (isset($args[0]['instagram']['id']) && !empty($args[0]['instagram']['id'])) {
            drupagram_account_save($args[0]['instagram']);
          }
        }

        $file_id = drupal_static('ufid');
        if ($file_id) {
          change_user_pic($result['uid'], $file_id);
          drupal_static_reset('ufid');
        }

        $result = new stdClass();
        $result->data = apiLogin($args[0]['name'], $args[0]['pass']);
      }
      break;

    case '_user_resource_update':
      $user_id = $result['uid'];
      if (isset($user_id)) {
        $result = user_load($user_id);
        api_services_account_object_alter($result);
        $file_id = drupal_static('ufid');
        if ($file_id) {
          change_user_pic($user_id, $file_id);
          drupal_static_reset('ufid');
          
          $file = file_load($file_id);
          $result->avatar = str_replace($GLOBALS['base_root'],'',url(file_create_url($file->uri)));
        }
        $result = array('data' => $result);
      }
      break;

    case '_taxonomy_term_resource_index':
      $success = TRUE;

      global $language;
      if ($language->langcode != 'en') {
        $data = _terms_translate($result, $language->langcode);
      }
      else {
        $data = $result;
      }
      $result = new stdClass();
      $result->data = $data;
      break;

    case 'services_views_execute_view':
      if (!empty($result) && count($result)) {
        $success = TRUE;

        $data = $result;
        $result = new stdClass();
        $data = api_views_structure($data, $args);

        $result->data = $data;
      }
      elseif($controller['view info']['view_name'] == 'products_api')
      {
        if (is_array($result))
          $result['data'] = null;
        else
          $result->data = null;
      }
      // In case of node not exists
      elseif($controller['view info']['view_name'] == 'product')
      {
        $success = TRUE;
        $result['data'] = null;
      }
      break;

    case 'commerce_services_product_create':
    case 'commerce_services_product_update':
    case 'api_commerce_services_product_update':
      if (isset($result->sku) && count($result)) {
        unset($_SESSION['product_id']);
        $data = api_product_create_response($result);
        $result = array();

        $result['data'] = $data;
        $success = TRUE;
      }
      break;
    case 'search_products':
      //custom method comes from file bartik/templates/views-view--search-api--block-1.tpl.php
      $success = TRUE;
      $result = array('data' => json_decode($result));
      break;
    case 'search_users':
      //custom method comes from file bartik/templates/views-view--search-api--block-1.tpl.php
      $success = TRUE;
      
      /*$out = json_decode($result);
      foreach($out->results as $one_rec)
      {
        if(!empty($one_rec->avatar))
        {
          $file = file_load($one_rec->avatar);
          $one_rec->avatar = str_replace($GLOBALS['base_root'],'',image_style_url('thumbnail',$file->uri));
        }
        
      }
      $result = array('data' => $out);*/
      
      if(count($result)){
        $current_page = $GLOBALS['pager_page_array'][0];
        $items_per_page = $GLOBALS['pager_limits'][0];
        $start = ($current_page) * $items_per_page + 1;
        $end = $start+count($result)-1; 

        $output = array(
          'results' => $result,
          'metadata' => array(
            'current_page' => $current_page,
            'total_pages' => $GLOBALS['pager_total'][0],
            'items_per_page' => $items_per_page,
            'display_start' => $start,
            'display_end' => $end,
            'total_results' => $GLOBALS['pager_total_items'][0],
          )
        );
      }
      else
        $output = array(
          'results' => array(),
          'metadata' => array());

      $result = array('data' => $output);
      break;
    case 'search_hashtags':
      $success = TRUE;
      $result = json_decode($result);
      foreach($result->results as $one_res){
        $posts_count = db_select('field_data_field_hashtags', 'u')
          ->fields('u')
          ->condition('field_hashtags_tid', $one_res->tid)
          ->countQuery()
          ->execute()
          ->fetchField();
        $one_res->posts_count = $posts_count;
      }
      $result = array('data' => $result);
      break;
    case '_services_votingapi_resource_set_votes':
      if(count($args[0]) == 1){
      $nid = $args[0][0]['entity_id'];
      
      $result = array('data' => 
        array('average' => $result['node'][$nid][1]['value'], 
        'count' => $result['node'][$nid][0]['value'],
      'user' => $args[0][0]['value']));
      
      }else{
        $result = array('data' => t('Backend have to handle more than one vote for request'));
      }
      break;

    case '_product_comment'://echo json_encode($result); exit;
      if (isset($result['cid']) || $result = $args[0]) {
        $success = TRUE;
        $result = array('data' => $result);
      }
      break;

    case '_comment_resource_update':
      $success = TRUE;
      $result = array('data' => array('cid' => $result));
      break;

    case '_comment_resource_retrieve':
      $success = TRUE;
      $result = array('data' => array('comment_body' => $result->comment_body['und'][0]['value'],
          'uid' => $result->uid));
      break;

    case '_system_resource_connect' :
      if (isset($result->user->uid) && $result->user->uid) {

        db_update('users')
            ->condition('uid', $result->user->uid)
            ->fields(array('login' => REQUEST_TIME))
            ->execute();
        $result->data = $result->user;
        unset($result->user);
        $success = TRUE;
      }
      break;

    case 'user_relationship_service_request'://echo json_encode($result->rid);exit;
      $success = isset($result->rid) ? TRUE : FALSE;
//        $data = $result;
      $result = array('data' => $result);
      break;

    case 'user_relationship_service_delete':
      $success = isset($result->deleted_by) ? TRUE : FALSE;
//        $data = $result;
      $result = array('data' => $result);
      break;

    case 'user_relationship_service_mine':
      $success = count($result);
      $result = array('data' => $result);
      break;

    case '_privatemsg_services_send':
      $success = isset($result['pmtid']) || $result ? TRUE : FALSE;
      $data = isset($result['messages']) ? $result['messages'] : NULL;
      $result = array('data' => $data[0]);

      break;

    case '_privatemsg_services_get_thread':
      $success = TRUE;
//        $participants_keys = array_keys($result['participants']);

      $result = array('data' => array('message' => $result['messages'],
//            'participants' => array(str_replace('user_', NULL, $participants_keys[0]), str_replace('user_', NULL, $participants_keys[1]))
      ));
      break;
    case 'commerce_services_cart_create' :
      $products = drupal_static('cart_products');
      drupal_static_reset('cart_products');
      $line_items = array();
      $total = 0;
      $currency = '';
      if (!empty($products)) {
        foreach ($products as $one_product) {
          $node_id = $one_product['commerce_product'];
          $one_product['type'] = 'product';
          $one_product['order_id'] = $result->order_id;
          $one_product['commerce_product'] = get_refernced_product_id($node_id);
          module_load_include('inc', 'commerce_services', 'resources/line_item');
          $line_item = commerce_services_line_item_create($one_product, 'true');
          $line_items[] = array('id' => $line_item->line_item_id, 'commerce_product' => $node_id, 'quantity' => $one_product['quantity'], 'unit_price' => $line_item->commerce_unit_price_formatted, 'price' => $line_item->commerce_total_formatted);
          $currency = $line_item->commerce_total['currency_code'];
          $total+= $line_item->commerce_total['amount'];
        }
      }
      $order = array(
        'id' => $result->order_id,
        'order_number' => $result->order_number,
        'status' => $result->status,
        'total' => $total . ' ' . $currency,
        'line_items' => $line_items
      );

      $result = $order;
      break;
    case '_comment_resource_create' :
      global $user;
      
      $node = node_load($args[0]['nid']);
      $notified_uid = $node->uid;

      // Send push notification
      $is_notified = '2';
      $prof = profile2_load_by_user($notified_uid,'notification_settings');
      if(!empty($prof->field_notification_comm_settings['und'][0]['value']) && !empty($prof->field_notification_status['und'][0]['value']))
      {
        if($prof->field_notification_status['und'][0]['value'] == '1')
        {
          $notify_flag = false;
          if($prof->field_notification_comm_settings['und'][0]['value'] == 3)
            $notify_flag = true;
          if($prof->field_notification_comm_settings['und'][0]['value'] == 2){
            module_load_include('inc', 'flag_service', 'flag_service');
            $flag_arr['flag_name'] = 'follow_user';
            $flag_arr['uid'] = $notified_uid;
            $flag_arr['entity_id'] = $user->uid;
            if(flag_service_is_flagged($flag_arr))
              $notify_flag = true;
          }

          if($notify_flag == true){
            $cnt = db_select('feeds', 'u')
              ->fields('u')
              ->condition('notified_uid', $notified_uid)
              ->condition('seen', '0')
              ->countQuery()
              ->execute()
              ->fetchField();

            $notify_msg = $user->name.' commented on your product';
            $is_sent = push_notifications_send_message(array($notified_uid),$notify_msg,array('count' => ($cnt+1),'Nalert'=>'1')); 
            $is_notified = '1';
            if($is_sent === FALSE)
              $is_notified = '0';
            elseif(empty($is_sent['count_success']))
              $is_notified = '0';
          }
        }
      }
      
      $seen = '0';
      if($is_notified == '2')$seen = '1';
        
      db_insert('feeds')
        ->fields(array(
          'uid'         => $user->uid,
          'entity_id'   => $args[0]['nid'],
          'feed_type'   => 'c',
          'notified_uid'=> $notified_uid,
          'created'     => REQUEST_TIME,
          'is_notified' => $is_notified,
          'cid'         => $result['cid'],
          'seen'        => $seen
        ))->execute();
      
      $success = TRUE;
      $data = $result;

      $result = new stdClass();
      $result->data = $data;
      break;
    case 'flag_service_flag_content':
      global $user;
      if($args[0]['action'] == 'flag' && ($args[0]['flag_name'] == 'follow_user' || $args[0]['flag_name'] == 'like')){
        $feed_type = 'f';
        $notified_uid = $args[0]['entity_id'];
        if($args[0]['flag_name'] == 'like')
        {
          $feed_type = 'l';
          $node = node_load($args[0]['entity_id']);
          $notified_uid = $node->uid;
        }
        
        // Send push notification
        $prof = profile2_load_by_user($notified_uid,'notification_settings');
        $is_notified = '2';
        if($feed_type == 'l')
        {
          // Send push notification
          if(!empty($prof->field_notification_like_settings['und'][0]['value']) && !empty($prof->field_notification_status['und'][0]['value']))
          {
            if($prof->field_notification_status['und'][0]['value'] == '1')
            {
              $notify_flag = false;
              if($prof->field_notification_like_settings['und'][0]['value'] == 3)
                $notify_flag = true;
              if($prof->field_notification_like_settings['und'][0]['value'] == 2)
              {
                module_load_include('inc', 'flag_service', 'flag_service');
                $flag_arr['flag_name'] = 'follow_user';
                $flag_arr['uid'] = $notified_uid;
                $flag_arr['entity_id'] = $user->uid;
                if(flag_service_is_flagged($flag_arr))
                  $notify_flag = true;
              }

              if($notify_flag == true){
                $cnt = db_select('feeds', 'u')
                  ->fields('u')
                  ->condition('notified_uid', $notified_uid)
                  ->condition('seen', '0')
                  ->countQuery()
                  ->execute()
                  ->fetchField();
                
                $notify_msg = $user->name.' liked your product';
                $is_sent = push_notifications_send_message(array($notified_uid),$notify_msg,array('count'=>($cnt+1),'Nalert'=>'1'));  
                $is_notified = '1';
                if($is_sent === FALSE)
                  $is_notified = '0';
                elseif(empty($is_sent['count_success']))
                  $is_notified = '0';
              }
            }
          }
        }
        else{
          // Send push notification
          if(!empty($prof->field_notification_follow_sett['und'][0]['value']) && !empty($prof->field_notification_status['und'][0]['value']))
          {
            if($prof->field_notification_status['und'][0]['value'] == '1')
            {
              $notify_flag = false;
              if($prof->field_notification_follow_sett['und'][0]['value'] == 1){
                $cnt = db_select('feeds', 'u')
                  ->fields('u')
                  ->condition('notified_uid', $notified_uid)
                  ->condition('seen', '0')
                  ->countQuery()
                  ->execute()
                  ->fetchField();

                $notify_msg = $user->name.' started following you';
                $is_sent = push_notifications_send_message(array($notified_uid),$notify_msg,array('count' => ($cnt+1),'Nalert'=>'1'));  
                $is_notified = '1';
                if($is_sent === FALSE)
                  $is_notified = '0';
                elseif(empty($is_sent['count_success']))
                  $is_notified = '0';
              }
            }
          }
        }
        
        $seen = '0';
        if($is_notified == '2')$seen = '1';
        db_insert('feeds')
        ->fields(array(
          'uid' => $user->uid,
          'entity_id' => $args[0]['entity_id'],
          'feed_type' => $feed_type,
          'notified_uid' => $notified_uid,
          'created' => REQUEST_TIME,
          'is_notified' => $is_notified,
          'seen' => $seen
        ))->execute();
        
      }
      elseif($args[0]['action'] == 'unflag' && ($args[0]['flag_name'] == 'follow_user' || $args[0]['flag_name'] == 'like'))
      {
        $feed_type = 'f';
        $notified_uid = $args[0]['entity_id'];
        if($args[0]['flag_name'] == 'like')
        {
          $feed_type = 'l';
          $node = node_load($args[0]['entity_id']);
          $notified_uid = $node->uid;
        }
        
        $query = db_delete('feeds');
        $query->condition('notified_uid', $notified_uid);
        $query->condition('uid', $user->uid);
        $query->condition('entity_id', $args[0]['entity_id']);
        $query->condition('feed_type', $feed_type);
        $query->execute();
        
        $cnt = db_select('feeds', 'u')
          ->fields('u')
          ->condition('notified_uid', $notified_uid)
          ->condition('seen', '0')
          ->countQuery()
          ->execute()
          ->fetchField();
        
        $notify_msg = 'hidden';
        push_notifications_send_message(array($notified_uid),$notify_msg,array('count' => $cnt,'Nalert' => '0'));  
      }
      $success = TRUE;
      $data = $result;

      $result = new stdClass();
      $result->data = $data;
      
      break;
    default :
      $success = TRUE;
      $data = $result;

      $result = new stdClass();
      $result->data = $data;
      break;
  }

  if (is_array($result)) {
    $result['success'] = $success;
  }
  else {
    $result->success = $success;
  }
  return $result;
}

/**
 * return translated term array
 * @param type $terms
 * @param type $lang
 * @return type array
 */
function _terms_translate($terms, $lang) {
  $data = array();
  foreach ($terms as $value) {

    $localized_term = i18n_taxonomy_term_name(taxonomy_term_load($value->tid), $lang);
    $data[] = array('name' => $localized_term, 'tid' => $value->tid, 'parent' => $value->parent);
  }
  return $data;
}
