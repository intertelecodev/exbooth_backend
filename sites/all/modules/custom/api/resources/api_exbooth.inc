<?php

/**
 * callback function for password reset
 * @param type $data
 */
function _api_password_reset($code, $pass) {
  $uid = variable_get($code, FALSE);

  if ($uid) {
    $latest_token = variable_get('latest_token_'.$uid, FALSE);
    if($latest_token != $code)
      return services_error(t('invalid key'), 200);
    variable_del($code);
    variable_del('latest_token_'.$uid);
    $user = user_load($uid);

    $deleted = db_delete('sessions')
        ->condition('uid', $uid)
        ->execute();

    if ($deleted) {
      module_invoke_all('user_logout', $user);
    }

    $user->pass = $pass;
    $edit = array('pass' => $pass, 'picture' => $user->picture, 'status' => TRUE, 'roles' => $user->roles);
    $edit['user'] = $user;
    $saved = user_save((object) array('uid' => $user->uid), $edit, 'Personal Information');
    echo apiLogin($user->name, $user->pass);
    exit;
  }
  else {
    return services_error(t('invalid key'), 200);
  }
}

/**
 * callback for search products
 */
function search_products($serchQuery) {
  if(trim($serchQuery['serchQuery'])==false && trim($serchQuery['categories'])==false && 
      trim($serchQuery['price_range'])==false && trim($serchQuery['country'])==false &&
      trim($serchQuery['type'])==false)
    return null;
  
  return views_embed_view('search_api', 'block_1', 
      !empty($serchQuery['serchQuery']) || strlen($serchQuery['serchQuery']) ? $serchQuery['serchQuery'] : '*****', 
      !empty($serchQuery['categories']) || strlen($serchQuery['categories']) ? $serchQuery['categories'] : '*****', 
      !empty($serchQuery['price_range']) || strlen($serchQuery['price_range']) ? $serchQuery['price_range'] : '*****',
      !empty($serchQuery['country']) || strlen($serchQuery['country']) ? $serchQuery['country'] : '*****',
      !empty($serchQuery['type']) || strlen($serchQuery['type']) ? $serchQuery['type'] : '*****');
}

/**
 * search users for api
 * @param type $search_query
 * @return type
 */
function search_users($serchQuery) {
  if(trim($serchQuery['serchQuery'])==false && trim($serchQuery['country'])==false)
    return null;
  if(empty($serchQuery['serchQuery']))$serchQuery['serchQuery']='*****';
  elseif(empty($serchQuery['country']))$serchQuery['country']='*****';

  $view = views_get_view('search_users');
  $view->set_arguments(array($serchQuery['serchQuery'],$serchQuery['country']),false);
  $view->set_display('services_1');
  $view->execute();
  $result = $view->result;
  $res=array();
  foreach($result as $one_res)
  {
    $avatar = '';
    if($one_res->users_picture)
    {
      $file = file_load($one_res->users_picture);
      $avatar = str_replace($GLOBALS['base_root'],'',file_create_url($file->uri));
    }
    $res[]=array(
      'uid' => $one_res->uid,
      'name'=> $one_res->users_name,
      'avatar' => $avatar
     );
  }
  return $res;
  //return views_embed_view('search_api_users', 'block', $serchQuery['serchQuery'],$serchQuery['country']);
}

/**
 * search users for api
 * @param type $search_query
 * @return type
 */
function search_hashtags($serchQuery) {
  if(strpos($serchQuery['serchQuery'],'#')===0)
    $serchQuery['serchQuery'] = substr ($serchQuery['serchQuery'], 1);
  if(!$serchQuery['serchQuery'] || strtolower($serchQuery['serchQuery'])=='all' || !trim($serchQuery['serchQuery']))$serchQuery['serchQuery']=NULL;
  
  return views_embed_view('hashtags_search', 'block', $serchQuery['serchQuery']);
}

/**
 * call back function to link instagram user to the current logged in user 
 */
function _instagram_user($instagram) {
  global $user;
  if (!isset($instagram['id']) || empty($instagram['id'])) {
    return services_error(t('Not instagram user'), 200);
  }
  $instagram['uid'] = $user->uid;
  module_load_include('inc', 'drupagram');
  try {
    drupagram_account_save($instagram);
    return drupagram_account_load($instagram['id']);
  }
  catch (Exception $e) {
    return services_error(t($e->getMessage()), 200);
  }
}

//call each function based on sent key
function _api_exbooth_get($key) {
  $func = '_get_' . $key;
  if (function_exists($func)) {
    return call_user_func($func);
  }
  else {
    return services_error('Not defined resource', 200);
  }
}

// get countries list and administrative_areas
function _get_countries() {
  module_load_include('inc', 'views', 'includes/cache');
  $countries = views_cache_get('countries');
  if($countries)
    return json_decode($countries->data);
  else{
    $view = views_get_view('countries');
    $view->set_display('services_1');
    $view->pre_execute();
    $view->execute();
    $view_results = $view->render();
    $prefix = array(
      'AF' => '93', 'AL' => '355', 'DZ' => '213', 'AS' => '1684', 'AD' => '376', 'AO' => '244', 'AI' => '1264', 'AG' => '1268', 'AR' => '54', 'AM' => '374', 'AW' => '297', 'AU' => '61', 'AT' => '43', 'AZ' => '994', 'BS' => '1242', 'BH' => '973', 'BD' => '880', 'BB' => '1246', 'BY' => '375', 'BE' => '32', 'BZ' => '501', 'BJ' => '229', 'BM' => '1441', 'BT' => '975', 'BO' => '591', 'BA' => '387', 'BW' => '267', 'BR' => '55', 'IO' => '246', 'BN' => '673', 'BG' => '359', 'BF' => '226', 'BI' => '257', 'KH' => '855', 'CM' => '237', 'CA' => '1', 'CV' => '238', 'KY' => '1345', 'CF' => '236', 'TD' => '235', 'CL' => '56', 'CN' => '86', 'CX' => '61', 'CC' => '672', 'CO' => '57', 'KM' => '269', 'CG' => '242', 'CD' => '243', 'CK' => '682', 'CR' => '506', 'CI' => '225', 'HR' => '385', 'CU' => '53', 'CY' => '357', 'CZ' => '420', 'DK' => '45', 'DJ' => '253', 'DM' => '1767', 'DO' => '1809', 'EC' => '593', 'EG' => '2', 'SV' => '503', 'GQ' => '240', 'ER' => '291', 'EE' => '372', 'ET' => '251', 'FK' => '500', 'FO' => '298', 'FJ' => '679', 'FI' => '358', 'FR' => '33', 'GF' => '594', 'PF' => '689', 'GA' => '241', 'GM' => '220', 'GE' => '995', 'DE' => '49', 'GH' => '233', 'GI' => '350', 'GR' => '30', 'GL' => '299', 'GD' => '1473', 'GP' => '590', 'GU' => '1671', 'GT' => '502', 'GN' => '224', 'GW' => '245', 'GY' => '592', 'HT' => '509', 'VA' => '39', 'HN' => '504', 'HK' => '852', 'HU' => '36', 'IS' => '354', 'IN' => '91', 'ID' => '62', 'IR' => '98', 'IQ' => '964', 'IE' => '353', 'IL' => '972', 'IT' => '39', 'JM' => '1876', 'JP' => '81', 'JO' => '962', 'KZ' => '7', 'KE' => '254', 'KI' => '686', 'KP' => '850', 'KR' => '82', 'KW' => '965', 'KG' => '996', 'LA' => '856', 'LV' => '371', 'LB' => '961', 'LS' => '266', 'LR' => '231', 'LY' => '218', 'LI' => '423', 'LT' => '370', 'LU' => '352', 'MO' => '853', 'MK' => '389', 'MG' => '261', 'MW' => '265', 'MY' => '60', 'MV' => '960', 'ML' => '223', 'MT' => '356', 'MH' => '692', 'MQ' => '596', 'MR' => '222', 'MU' => '230', 'YT' => '269', 'MX' => '52', 'FM' => '691', 'MD' => '373', 'MC' => '377', 'MN' => '976', 'MS' => '1664', 'MA' => '212', 'MZ' => '258', 'MM' => '95', 'NA' => '264', 'NR' => '674', 'NP' => '977', 'NL' => '31', 'AN' => '599', 'NC' => '687', 'NZ' => '64', 'NI' => '505', 'NE' => '227', 'NG' => '234', 'NU' => '683', 'NF' => '672', 'MP' => '1670', 'NO' => '47', 'OM' => '968', 'PK' => '92', 'PW' => '680', 'PS' => '970', 'PA' => '507', 'PG' => '675', 'PY' => '595', 'PE' => '51', 'PH' => '63', 'PL' => '48', 'PT' => '351', 'PR' => '1787', 'QA' => '974', 'RE' => '262', 'RO' => '40', 'RU' => '7', 'RW' => '250', 'SH' => '290', 'KN' => '1869', 'LC' => '1758', 'PM' => '508', 'VC' => '1784', 'WS' => '684', 'SM' => '378', 'ST' => '239', 'SA' => '966', 'SN' => '221', 'SC' => '248', 'SL' => '232', 'SG' => '65', 'SK' => '421', 'SI' => '386', 'SB' => '677', 'SO' => '252', 'ZA' => '27', 'ES' => '34', 'LK' => '94', 'SD' => '249', 'SR' => '597', 'SJ' => '47', 'SZ' => '268', 'SE' => '46', 'CH' => '41', 'SY' => '963', 'TW' => '886', 'TJ' => '992', 'TZ' => '255', 'TH' => '66', 'TL' => '670', 'TG' => '228', 'TK' => '690', 'TO' => '676', 'TT' => '1868', 'TN' => '216', 'TR' => '90', 'TM' => '7370', 'TC' => '1649', 'TV' => '688', 'UG' => '256', 'UA' => '380', 'AE' => '971', 'GB' => '44', 'US' => '1', 'UM' => '1', 'UY' => '598', 'UZ' => '998', 'VU' => '678', 'VE' => '58', 'VN' => '84', 'VG' => '1284', 'VI' => '1340', 'WF' => '681', 'EH' => '212', 'YE' => '967', 'ZM' => '260', 'ZW' => '263', 'RS' => '381', 'ME' => '382', 'AX' => '358', 'BQ' => '599', 'CW' => '599', 'GG' => '44', 'IM' => '44', 'JE' => '44', 'XK' => '381', 'BL' => '590', 'MF' => '590', 'SX' => '1', 'SS' => '211',
    );
    foreach ($view_results as $result) {
      $result->prefix = '';
      if(isset($prefix[$result->code]))
        $result->prefix = $prefix[$result->code];
    }
    views_cache_set('countries', json_encode($view_results));
    return $view_results;
  }
}

function _get_product_types() {
  return module_invoke('commerce_product_ui', 'commerce_product_type_info');
}

/**
 * get all product categories grouped by vocabulary name.
 * optionaly return 5 images for products inside each term.
 * cached for 30 minutes.
 * @todo decrease cache interval.
 */
function _get_all_products_taxonomies() {
  //variables 
  $images = isset($_REQUEST['images']) ? intval($_REQUEST['images']) : 0;
  $page = 0;
  $page_size = variable_get('services_taxonomy_term_index_page_size', 20);

  $cid = 0;
  //cached ? return : complete.
  if (FALSE) {
    $cid = 'all_products_taxonomies' . $page_size . $page;
    $cached_data = cache_get($cid);
    if ($cached_data) {
      return $cached_data->data;
    }
  }

//product types then field names _category
  $product_types = module_invoke('commerce_product_ui', 'commerce_product_type_info');
  $product_category_types = array();
  foreach ($product_types as $k => $v) {
    $product_category_types[] = $k . '_category';
  }

//get vocabularies matched with product types
  $produc_categories = db_select('taxonomy_vocabulary', 'tv')
      ->fields('tv', array('vid', 'name', 'machine_name'))
      ->condition('machine_name', $product_category_types, 'IN')
      ->execute()
      ->fetchAll();

  $data = array();
  // get taxonomy terms grouped with product types 
  foreach ($produc_categories as $key => $value) {
    $query = db_select('taxonomy_term_data', 't')->fields('t', array('name'));
    //authenticated user get flagged terms
    if ($GLOBALS['user']->uid) {
      $query->leftJoin('flagging', 'f', 'f.entity_id = t.tid AND f.entity_type = :taxonomy_term AND f.uid = :uid',
          array(':taxonomy_term' => 'taxonomy_term', ':uid' => $GLOBALS['user']->uid));
      $query->addField('f', 'flagging_id', 'followed');
    }
    
    $query->join('taxonomy_term_hierarchy', 'tth', 't.tid = tth.tid');
    $query->fields('tth', array('tid', 'parent'));

    //subquery for counter to order terms by
    $count = '(SELECT COUNT(*) FROM field_data_field_category tc WHERE tc.field_category_target_id = t.tid)';
    $query->addExpression($count, 'counter');
    
    $query->condition('t.vid', $value->vid)
        ->orderBy('counter', 'DESC');

    $query->range($page * $page_size, $page_size);
    $result = $query->execute()->fetchAll();
    
    // get images for 5 products in each term
    foreach ($result as $k => $v) {
      $v->name = t($v->name);
      $type = str_replace('_category', NULL, $value->machine_name);

      if ($images) {
        $query = db_select('field_data_field_category', 't1');

        $query->innerJoin('field_data_field_images', 't2', 't2.entity_id = t1.entity_id AND t2.delta = 0');
        
        $query->innerJoin('file_managed', 't4', 't4.fid = t2.field_images_fid');

        $query->innerJoin('field_data_field_product', 'fp', 'fp.field_product_product_id = t1.entity_id');
        
        $query->innerJoin('node', 'n', 'n.nid = fp.entity_id AND n.status = 1 AND n.type = :product',
            array(':product' => 'product'));
       
        $query->leftJoin('flag_counts', 'fc', 'fc.entity_id = fp.entity_id AND fp.entity_type = :node',
            array(':node' => 'node'));
        
        $query->leftJoin('flag', 'f', 'f.fid = fc.fid AND f.name = :flag_name AND f.entity_type = :node',
            array(':flag_name' => 'like'));

        $query->condition('t1.field_category_target_id', $v->tid);
        $query->condition('t1.entity_type', 'commerce_product');

        $query->addField('n', 'nid', 'product_id');
        $query->fields('t4', array('uri'));
        $query->distinct();

        $query->orderBy('fc.count', 'DESC');
        $query->orderBy('n.changed', 'DESC');
        //@TODO: sort by most viewed.

        $query->range(0, 5);
        $items = $query->execute()->fetchAll();
        if (count($items)) {
//      echo '<pre>'.$v->tid; print_r($query->execute()->queryString); exit;
          $products = array();
          foreach ($items as $item) {
            $products[] = array('product_id' => $item->product_id, 'image' => file_create_url($item->uri));
          }
          $v->products = $products;
        }
      }
      $data[$type][] = $v;
    }
    
  }
  
  //add types arranged as the design product, service, exhibition.
    foreach ($product_types as $k=>$v)
    {
      $product_types[$k]['name'].= 's';
    }
    $types = array_values($product_types);
    $data['types'] = array($types[1], $types[2], $types[0]);
  //set cache
  if ($cid) {
    $interval = 60 * 60 * 5;
    cache_set($cid, $data, 'cache', $interval);
  }
  return $data;
}

function delete_multiple_comments($data) {
  $ids = explode(',',$data['ids']);
  $comment = comment_load($ids[0]);
  $node = node_load($comment->nid);
  $notified_uid = $node->uid;
        
  $query = db_delete('feeds');
  $query->condition('feed_type', 'c');
  $query->condition('cid', $ids, 'IN');
  $query->execute();

  $cnt = db_select('feeds', 'u')
    ->fields('u')
    ->condition('notified_uid', $notified_uid)
    ->condition('seen', '0')
    ->countQuery()
    ->execute()
    ->fetchField();

  $notify_msg = 'hidden';
  push_notifications_send_message(array($notified_uid),$notify_msg,array('count' => $cnt,'Nalert' => '0'));  
  
  comment_delete_multiple($ids);
  return true;
}

function delete_multiple_comments_access($data) {
  global $user;
  $ids = explode(',',$data['ids']);
  foreach ($ids as $one_id)
  {
    $com_obj = comment_load($one_id);
    if($com_obj->uid != $user->uid)
      return false;
  }
  return true;
}

// Notification settings
function _get_notification_settings() {
  global $user;
  if(empty($user->uid))
    return null;
  $prof = profile2_load_by_user($user->uid,'notification_settings');
  if($prof)
  {
    $out = array();
    $out['status'] = $prof->field_notification_status['und'][0]['value'];
    $out['like_settings'] = $prof->field_notification_like_settings['und'][0]['value'];
    $out['comment_settings'] = $prof->field_notification_comm_settings['und'][0]['value'];
    $out['follow_settings'] = $prof->field_notification_follow_sett['und'][0]['value'];
    if(!$out['follow_settings'])$out['follow_settings'] = '2';
    return $out;
  }
  else
  {
    $out = $prof = array();
    $out['status'] = $prof['field_notification_status']['und'][0]['value'] = '1';
    $out['like_settings'] = $prof['field_notification_like_settings']['und'][0]['value'] = '3';
    $out['comment_settings'] = $prof['field_notification_comm_settings']['und'][0]['value'] = '3';
    $out['follow_settings'] = $prof['field_notification_follow_sett']['und'][0]['value'] = '1';
    $prof['type'] = 'notification_settings';
    $prof['uid'] = $user->uid;
    profile2_create($prof)->save();
    return $out;
  }
}

function notification_settings($data) {
  global $user;
  if(empty($user->uid))
    return null;
  $prof = profile2_load_by_user($user->uid,'notification_settings');
  if($prof)
  {
    $prof->field_notification_status['und'][0]['value'] = $data['status'];
    $prof->field_notification_like_settings['und'][0]['value'] = $data['like_settings'];
    $prof->field_notification_comm_settings['und'][0]['value'] = $data['comment_settings'];
    $prof->field_notification_follow_sett['und'][0]['value'] = $data['follow_settings'];
    $prof->field_device_type['und'][0]['value'] = $data['device_type'];
    $prof->field_token['und'][0]['value'] = $data['token'];
    profile2_save($prof);
  }
  else{
    $prof = array();
    $prof['field_notification_status']['und'][0]['value'] = $data['status'];
    $prof['field_notification_like_settings']['und'][0]['value'] = $data['like_settings'];
    $prof['field_notification_comm_settings']['und'][0]['value'] = $data['comment_settings'];
    $prof['field_notification_follow_sett']['und'][0]['value'] = $data['follow_settings'];
    $prof['field_device_type']['und'][0]['value'] = $data['device_type'];
    $prof['field_token']['und'][0]['value'] = $data['token'];
    $prof['type'] = 'notification_settings';
    $prof['uid'] = $user->uid;
    profile2_create($prof)->save();
  }
  
  // Delete stored tokens for current user
  $query = db_delete('push_notifications_tokens');
  $query->condition('uid', $user->uid);
  $query->execute();
  
  $query = db_delete('push_notifications_tokens');
  $query->condition('token', $data['token']);
  $query->execute();
  
  // Store new token
  push_notifications_store_token($data['token'], $data['device_type'], $user->uid);
  
  $cnt = db_select('feeds', 'u')
    ->fields('u')
    ->condition('notified_uid', $user->uid)
    ->condition('is_notified', '0')
    ->countQuery()
    ->execute()
    ->fetchField();
  
  if($cnt)
  {
    db_update('feeds')
      ->condition('notified_uid', $user->uid)
      ->fields(array('is_notified' => '1'))
      ->execute();
    
    $notify_msg = 'You have '.$cnt.' new notification';
    if($cnt != 1)$notify_msg.='s';
    push_notifications_send_message(array($user->uid),$notify_msg,array('count' => $cnt,'Nalert'=>'1')); 
  }
  unset($data['device_type']);
  unset($data['token']);
  return $data;
}

function notification_settings_access($data) {
  global $user;
  if(!$user || empty($data['status']) || empty($data['like_settings']) || empty($data['comment_settings'])|| empty($data['device_type'])|| empty($data['token']))
    return false;
  return true;
}

function _get_feeds() {
  global $user;
  $output = $paged_output = array();
  if(!empty($user->uid))
  {
    db_update('feeds')
      ->condition('notified_uid', $user->uid)
      ->fields(array('seen' => '1'))
      ->execute();

    $current_page = !empty($_GET['page']) ? $_GET['page'] : 0;
    $items_per_page = 10;
    
    $flag = flag_get_flag('follow_user');
    
    $result = db_query("SELECT * FROM {feeds} where notified_uid='$user->uid' order by created desc limit ".($current_page*$items_per_page).",$items_per_page");//$user->uid
    $feeds_res = $result->fetchAll();
    
    
    $total_results = db_select('feeds', 'u')
      ->fields('u')
      ->condition('notified_uid', $user->uid)
      ->countQuery()
      ->execute()
      ->fetchField();
    
    if ($items_per_page>0&&$total_results>$items_per_page) {
      $total_pages = $total_results/$items_per_page-1;
    }
    else {
      $total_pages=1;
    }
    
    if(count($feeds_res)){
      $start = ($current_page) * $items_per_page + 1;
      $end = $start+count($feeds_res)-1; 
    }
    else{
      $start = $end = 0;
    }
    $paged_output = array(
      'metadata' => array(
      'current_page' => $current_page,
      'total_pages' => ceil($total_pages),
      'items_per_page' => $items_per_page,
      'display_start' => $start,
      'display_end' => $end,
      'total_results' => $total_results),
      'results' => $output
    );

    foreach ($feeds_res as $record) {
      $out = array();
      $out['id'] = $record->id;
      $out['type'] = 'like';
      $out['action'] = 'Liked your product';
      if($record->feed_type == 'c'){
        $out['type'] = 'comment';
        $out['action'] = 'Commented on your product';
      }
      elseif($record->feed_type == 'f'){
        $out['type'] = 'follow';
        $out['action'] = 'Started following you';
      }
      $time_elapsed = time() - $record->created;
      $seconds 	= $time_elapsed ;
      $minutes 	= floor($time_elapsed / 60 );
      $hours 		= floor($time_elapsed / 3600);
      $days 		= floor($time_elapsed / 86400 );
      $weeks 		= floor($time_elapsed / 604800);
      $years 		= floor($time_elapsed / 31207680 );

      if($years)$out['created'] = $years.'y';
      elseif($weeks)$out['created'] = $weeks.'w';
      elseif($days)$out['created'] = $days.'d';
      elseif($hours)$out['created'] = $hours.'h';
      elseif($minutes)$out['created'] = $minutes.'m';
      elseif($seconds)$out['created'] = $seconds.'s';
    
      $out['author_uid'] = $record->uid;
      $user_arr = user_load ($record->uid);
      $out['author_image'] = '';
      if(!empty($user_arr->picture->uri))
        $out['author_image'] = str_replace($GLOBALS['base_root'],'',file_create_url($user_arr->picture->uri));
      $out['author_name'] = $user_arr->name;
      
      $out['is_followed'] = '0';
      $out['is_following'] = '0';
      $out['is_deleted'] = '0';
      $out['product_image'] = '';
      $out['product_id'] = '';
      if($record->feed_type == 'c' || $record->feed_type == 'l'){
        $out['product_id'] = $record->entity_id;
        $product_id = get_refernced_product_id($record->entity_id);
        $commerce_product = commerce_product_load($product_id);
        
        if(!empty($commerce_product->field_images['und'][0]['uri']))
          $out['product_image'] = str_replace($GLOBALS['base_root'],'',file_create_url($commerce_product->field_images['und'][0]['uri']));
        if(!$commerce_product)
          $out['is_deleted'] = '1';
      }
        
      $flagged = $flag->is_flagged($record->uid,$user->uid);
      if($flagged === true)
        $out['is_followed'] = '1';
      
      $flagged = $flag->is_flagged($user->uid,$record->uid);
      if($flagged === true)
        $out['is_following'] = '1';
      
      $output[] = $out;
    }
    $paged_output['results'] = $output;
  }
  return $paged_output;
}

function _get_push_test()
{
  global $user;
  push_notifications_send_message(array($user->uid),'Test push notification');
}

function _get_currencies()
{
  $currencies = array();
  $curr_arr = currency_load_all();
  foreach($curr_arr as $one_curr)
  {
    $currencies[] = array(
      'name' => $one_curr->title,
      'code' => $one_curr->ISO4217Code
    );
  }
  return $currencies;
}

function _get_check_login() {
  global $user;
  if(!$user->uid)return false;
  return true;
}

function push_notification_update_token($data) {
  global $user;
  // Delete stored tokens for current user
  $query = db_delete('push_notifications_tokens');
  $query->condition('uid', $user->uid);
  $query->execute();
  
  $query = db_delete('push_notifications_tokens');
  $query->condition('token', $data['token']);
  $query->execute();
  
  // Store new token
  push_notifications_store_token($data['token'], $data['device_type'], $user->uid);
  return true;
}

function push_notification_update_token_access($data) {
  global $user;
  if(empty($data['token']) || !$user->uid)
    return false;
  return true;
}

function upload_item_images_access($data) {
  global $user;
  if(!$user->uid)
    return false;
  return true;
}
function upload_item_images(){
  $file = array();
  foreach ($_FILES as $k=>$v)
  {
    $file_name = microtime(TRUE).'.'.end(explode('.', $_FILES[$k]["name"]));
    move_uploaded_file($_FILES[$k]["tmp_name"], 'tmpp/'.$file_name);
    $file[]='/tmpp/'.$file_name;
  }
  return $file;
}