<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * password reset callback to send mail to the user with token to reset password
 */
function api_reset_password($name_or_mail) {
  $field = valid_email_address($name_or_mail) ? 'mail' : 'name';
  $users = user_load_multiple(array(), array($field => $name_or_mail, 'status' => '1'));

  watchdog('api_reset_password', json_encode(array('field' => $field, 'value' => $name_or_mail,
    'token' => token_replace('[exbooth:reset_password_token]'))));  
  
  $account = reset($users);
  if (!isset($account->uid)) {
    return services_error(t('Sorry, %name is not recognized as a user name or an e-mail address.', array('%name' => $name)), 406);
  }
  
  $avatar = '';
  if(isset($account->picture->uri))
    $avatar = str_replace($GLOBALS['base_root'],'',file_create_url($account->picture->uri));
  $user_data = array(
    'id'   => $account->uid,
    'name' => $account->name,
    'avatar'=> $avatar,
    'mail' => $account->mail
  );
  
  $pet = pet_load('reset_password');
  $data = array(
    'pet_to' => $account->mail,
    'pet_from' => token_replace('[site:mail]'),
  );
  $pet->mail_body = str_replace('{user_name}',$account->name , $pet->mail_body);
  $sent_mail = pet_send_one_mail($pet, $data);
  watchdog('api', 'args %name', array('%name' => json_encode($sent_mail)), WATCHDOG_ALERT);
  if ($sent_mail) {
    
    $token = token_replace('[exbooth:reset_password_token]');
    variable_set($token, $account->uid);
    variable_set($token . reset_password_token_creation_time, time());
    variable_del('latest_token_'.$account->uid);
    variable_set('latest_token_'.$account->uid, $token);
    
    echo json_encode(array('success' => TRUE, 'data' => $user_data));
    exit;
  }
  else {
    watchdog('api', 'reset pass mail not sent : args %name', array('%name' => json_encode($sent_mail)), WATCHDOG_ALERT);
    services_error(t('Sorry the mail did not sent please try again later'), 200);
  }
}

/**
 * 
 * @param type $uid Integer
 * @return type Array
 * get user products.
 */
function api_get_user_products($uid) {
  $view = views_get_view('products_api');

  $view->set_display('services_2');
  $view->set_arguments(array($uid));

  $view->execute();
  $result = array();

  foreach ($view->result as $value) {
    $obj = $value->_field_data['product_id']['entity'];

    $data = array();
    foreach ($obj->field_images['und'] as $fil) {

      $file = file_load($fil['fid']);
      $data['productImageUrls'][] = array('baseImage' => image_style_url('large', $fil['uri']), 'thumbnail' => image_style_url('300x300', $fil['uri']));
    }
    $data['favorite'] = $value->flagging_flagged;
    $data['favorite_counter'] = isset($value->flag_counts_node_count) ? $value->flag_counts_node_count : 0;

//    echo json_encode($view->result); exit;
    // get comments on the product see admin_api.module
    $data['comments'] = product_comments($value->field_product_commerce_product_nid);
    $data['productId'] = $value->product_id;

    $data['uid'] = $obj->revision_uid;
    $data['productCaption'] = $obj->field_caption['und'][0]['value'];

    $data['productTitle'] = $obj->title;
    $data['productPrice'] = $obj->commerce_price['und'][0]['amount'];

    $data['priceCurrency'] = $obj->commerce_price['und'][0]['currency_code'];
    $data['categoryId'] = $obj->field_category['und'][0]['tid'];

    $data['createdDate'] = $obj->created;
    $data['lastUpdatedDate'] = $obj->changed;

    $data['categoryName'] = $value->taxonomy_term_data_field_data_field_category_name;
    $result[] = $data;
  }
  return array('total' => $view->total_rows, 'products' => $result);
}
