<?php

/*
 * Post process hook Implementation
 */

/**
 * Implements hook_postprocess_alter().
 * @param type $controller
 * @param type $args
 * @param type $result
 */
function api_request_parsed_alter(&$data, $controller) {
  switch ($controller['callback']) {
    case 'commerce_services_product_create':
    case 'commerce_services_product_update':
    case 'api_commerce_services_product_update':
      if (isset($data['field_geoloc'])) {
        $data["field_geoloc"] = geofield_compute_values($data['field_geoloc']['und'][0]['geom']);
      }

      if (isset($data['field_direct_contact'])) {
        $_SESSION['field_direct_contact'] = $data['field_direct_contact'];
        unset($data['field_direct_contact']);
      }

      if (isset($data['expire'])) {
        $_SESSION['expire'] = $data['expire'];
        unset($data['expire']);
      }
      break;

    case '_services_votingapi_resource_set_votes':
      $data = array('votes' => array($data));
      break;
    case '_user_resource_login':
      global $user;
      if(!empty($user->uid))
      {
        module_invoke_all('user_logout', $user);
        session_destroy();
      }
    break;
  }
}
