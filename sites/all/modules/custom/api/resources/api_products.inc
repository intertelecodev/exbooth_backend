<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * create product response for services
 */
function api_product_create_response($result){
    $data = new stdClass();
    $data->productId = $result->nid;
    
    $data->images = array();
    foreach ($result->field_images as $fil) {

      $file = file_load($fil['fid']);
      $data->images[] = str_replace($GLOBALS['base_root'],'',file_create_url($file->uri));
    }
    $data->uid = $result->revision_uid;
    $data->productCaption = $result->field_caption['value'];
    
    $data->prodcutTitle = $result->title;
    //$data->productPrice = $result->commerce_price['amount'];
    
    $curr_info = currency_load($result->commerce_price['currency_code']);
    $data->productPrice = $result->commerce_price['amount']/$curr_info->subunits;
      
    $data->priceCurrency = $result->commerce_price['currency_code'];
    $category_field = 'field_category';
    $data->categoryId = $result->$category_field;
 
    $data->createdDate = $result->created;
    $data->lastUpdatedDate = $result->changed;
    $data->field_geoloc = $result->field_geoloc;
    $data->type = $result->type;
    //@TODO: fix issue with geophp field with commerce
    
    /*if(isset($result->type)){
     $geoloc_field = 'field_' . $result->type . '_geoloc';
    if(isset($result->$geoloc_field)){
      $data->field_geoloc = $result->$geoloc_field;
    }
   }*/
    return $data;
}