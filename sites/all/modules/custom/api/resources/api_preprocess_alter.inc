<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function api_preprocess_alter($controller, &$args, $options) {
  switch ($controller['callback']) {
    case '_user_resource_create':
      $args[0]['field_gender'] = array('und' => array('value' => $args[0]['gender']));
      $args[0]['field_mobile']['und'][0]['value'] = $args[0]['phone'];
      $args[0]['field_username']['und'][0]['value'] = $args[0]['username'];
      $args[0]['status'] = 1;

      $args[0]['field_country']['und'] = array('iso2'=>$args[0]['country']);
      $args[0]['field_website']['und'][0]['value'] = $args[0]['website'];
      if (!empty($args[0]['avatar'])) {
        $file_name = time().'.';
        $file = upload_img($args[0]['avatar'], $file_name, 'users/');

        drupal_static_reset('ufid');
        drupal_static('ufid', $file->fid);
      }
      
      if (!empty($args[0]['cover'])) {
        $file = upload_img($args[0]['cover'], time().'.');
        $file2 = $file;
        $file2->status = 0;
        file_save($file2);
        $args[0]['field_cover']['und'][0] = array('fid' => $file->fid);
      }
      
      $args[0]['field_caption']['und'][0]['value'] = $args[0]['bio'];
      break;

    case '_user_resource_update':
      global $user;
      $args[1]['uid'] = $args[0];
      if (!empty($args[1]['avatar'])) {
        $file_name = time().'.';
        $file = upload_img($args[1]['avatar'], $file_name, 'users/');

        drupal_static_reset('ufid');
        drupal_static('ufid', $file->fid);
      }
      if (isset($args[1]['phone'])) {
        $args[1]['field_mobile']['und'][0]['value'] = $args[1]['phone'];
      }
      if (isset($args[1]['gender'])) {
        $args[1]['field_gender'] = array('und' => array('value' => $args[1]['gender']));
      }
      if (isset($args[1]['username'])) {
        $args[1]['field_username']['und'][0]['value'] = $args[1]['username'];
      }
      if (isset($args[1]['country'])) {
        $args[1]['field_country']['und'] = array('iso2'=>$args[1]['country']);
      }
      if (!empty($args[1]['cover'])) {
        $file = upload_img($args[1]['cover'], time().'.');
        file_usage_add($file, 'user', 'user', $args[1]['uid']);
        $args[1]['field_cover']['und'][0] = array('fid' => $file->fid);
      }
      if (isset($args[1]['bio'])) {
        $args[1]['field_caption']['und'][0]['value'] = $args[1]['bio'];
      }
      if (isset($args[1]['website'])) {
        $args[1]['field_website']['und'][0]['value'] = $args[1]['website'];
      }
      break;

    case 'commerce_services_product_create':
      foreach ($args[0]['productImages'] as $value) {
        $file_name = microtime(TRUE).'.';
        $file = upload_img($value, $file_name);
        
        if (isset($file->fid)) {
          $args[0]['field_images'][] = array('fid' => $file->fid, 'width' => 100, 'height' => 100, 'alt' => $args[0]['productTitle'], 'title' => $args[0]['productTitle']);
        }
      }

      $args[0]['sku'] = isset($args[0]['sku']) ? $args[0]['sku'] : time().rand(10000,99999);
      $args[0]['title'] = $args[0]['productTitle'];

      if(empty($args[0]['productPrice']))$args[0]['productPrice']=0;
      if(empty($args[0]['priceCurrency']))$args[0]['priceCurrency']='KWD';
      $curr_info = currency_load($args[0]['priceCurrency']);
      $args[0]['productPrice'] = $args[0]['productPrice']*$curr_info->subunits;
        
      $args[0]['commerce_price'] = array('amount' => $args[0]['productPrice'], 'currency_code' => $args[0]['priceCurrency']);
      $args[0]['field_caption']['value'] = $args[0]['productCaption'];

      $args[0]['field_category'] = $args[0]['categoryId'];
//
		$args[0]['field_country'] = array('iso2'=>$args[0]['productCountry']);
		$args[0]['field_city']['value'] = $args[0]['productCity'];
		$args[0]['field_product_quantity']['value'] = $args[0]['productQuantity'];

    if(!empty($args[0]['field_exhibition_date']))
    {
        $date_arr = explode(" to ",$args[0]['field_exhibition_date']);
        if(!empty($date_arr[0]) && !empty($date_arr[1]))
        {
          $args[0]['field_exhibition_date'] =  array(
            "value"=>$date_arr[0],
            "value2"=>$date_arr[1]
          );
        }
    }
    
    if(!empty($args[0]['author_mobile']))
      $args[0]['field_mobile']['value'] = $args[0]['author_mobile'];
    
      unset($args[0]['productImages']);
      unset($args[0]['productPrice']);
//        
      unset($args[0]['categoryId']);
      unset($args[0]['productTitle']);
//
      unset($args[0]['priceCurrency']);
      unset($args[0]['productCaption']);
		
		unset($args[0]['productCountry']);
		unset($args[0]['productCity']);
		unset($args[0]['productQuantity']);
    unset($args[0]['author_mobile']);

      break;

    case 'commerce_services_product_update':
    case 'api_commerce_services_product_update':
      foreach ($args[1]['productImages'] as $value) {
        $file_name = microtime(TRUE).'.';
        $file = upload_img($value, $file_name);
        if (isset($file->fid)) {
          $args[1]['field_images'][] = array('fid' => $file->fid, 'width' => 100, 'height' => 100, 'alt' => $args[1]['productTitle'], 'title' => $args[1]['productTitle']);
        }
      }
      if($args[1]['productImages'])
        api_delete_files(get_refernced_product_id($args[0]), 'commerce_product');
      
      if(!empty($args[1]['productTitle']))
        $args[1]['title'] = $args[1]['productTitle'];
      if(!empty($args[1]['productPrice']))
      {
        $curr_info = currency_load($args[1]['priceCurrency']);
        $args[1]['productPrice'] = $args[1]['productPrice']*$curr_info->subunits;
        $args[1]['commerce_price'] = array('amount' => $args[1]['productPrice'], 'currency_code' => $args[1]['priceCurrency']);
      }
      
      if(!empty($args[1]['productCaption']))
        $args[1]['field_caption'] = array('value' => $args[1]['productCaption']);
      if(!empty($args[1]['categoryId']))
        $args[1]['field_category'] = $args[1]['categoryId'];
//
		if(!empty($args[1]['productCountry']))
      $args[1]['field_country'] = array('iso2'=>$args[1]['productCountry']);
		if(!empty($args[1]['productCity']))
      $args[1]['field_city']['value'] = $args[1]['productCity'];
		if(!empty($args[1]['productQuantity']))
      $args[1]['field_product_quantity']['value'] = $args[1]['productQuantity'];
		
    if(!empty($args[1]['field_exhibition_date']))
    {
        $date_arr = explode(" to ",$args[1]['field_exhibition_date']);
        if(!empty($date_arr[1]) && !empty($date_arr[1]))
        {
          $args[1]['field_exhibition_date'] =  array(
            "value"=>$date_arr[0],
            "value2"=>$date_arr[1]
          );
        }
    }
    
    if(!empty($args[1]['author_mobile']))
      $args[1]['field_mobile']['value'] = $args[1]['author_mobile'];
    
      unset($args[1]['productImages']);
      unset($args[1]['productPrice']);
//        
      unset($args[1]['categoryId']);
      unset($args[1]['productTitle']);
//
      unset($args[1]['priceCurrency']);
      unset($args[1]['productCaption']);
	  
	  unset($args[1]['productCountry']);
	  unset($args[1]['productCity']);
	  unset($args[1]['productQuantity']);
    unset($args[1]['author_mobile']);
      break;

    case '_user_resource_request_new_password':
      api_reset_password($args[0]);
      break;

    case '_comment_resource_update':
      $args[1]['comment_body'] = array('und' => array(array('value' => $args[1]['comment_body'])));
      break;

    case '_comment_resource_create':
      $args[0]['comment_body'] = array('und' => array(array('value' => $args[0]['comment_body'])));
      break;
    
    case '_comment_resource_delete' :
      global $user;
      $comment = comment_load($args[0]);

      if($comment)
      {
        $node = node_load($comment->nid);
        $notified_uid = $node->uid;
      
        $query = db_delete('feeds');
        $query->condition('notified_uid', $notified_uid);
        $query->condition('uid', $user->uid);
        $query->condition('entity_id', $comment->nid);
        $query->condition('cid', $args[0]);
        $query->condition('feed_type', 'c');
        $query->execute();

        $cnt = db_select('feeds', 'u')
          ->fields('u')
          ->condition('notified_uid', $notified_uid)
          ->condition('seen', '0')
          ->countQuery()
          ->execute()
          ->fetchField();

        $notify_msg = 'hidden';
        push_notifications_send_message(array($notified_uid),$notify_msg,array('count' => $cnt,'Nalert' => '0'));  
      }
      break;
      
    case 'commerce_services_line_item_create':
      $args[0]['commerce_product'] = get_refernced_product_id($args[0]['commerce_product']);
      break;

    case 'commerce_services_cart_create':
      drupal_static_reset('cart_products');
      drupal_static('cart_products', $args[0]['products']);
      unset($args[0]['products']);
      break;
    
    case 'flag_service_flag_content':
      if($args[0]['action'] == 'flag')
      {
        global $user;
        module_load_include('inc', 'flag_service', 'flag_service');
        $flag_arr['flag_name'] = $args[0]['flag_name'];
        $flag_arr['uid'] = $user->uid;
        $flag_arr['entity_id'] = $args[0]['entity_id'];
        if(flag_service_is_flagged($flag_arr))
        {
          echo json_encode (array('data'=>false,'success'=>true));
          exit;
        }
      }
      
      if($args[0]['flag_name'] == 'inappropriate' && isset($args[0]['reason']))
      {
        $args[0]['flagging']['field_caption'] = array('und' => array(array('value' => $args[0]['reason'])));
        unset($args[0]['reason']);
      }
      break;
    case '_user_resource_logout' :
      global $user;
      // Delete stored tokens for current user
      $query = db_delete('push_notifications_tokens');
      $query->condition('uid', $user->uid);
      $query->execute();
      break;
    case '_user_resource_login':
      $args[1] = trim($args[1]);
      break;
  }
  return $args;
}
