<?php

/**
 * definition of custom resources.
 */
function custom_resources_definition() {
    return array(
        'exbooth' => array(
            'operations' => array(
                'retrieve' => array(
                    'help' => 'Retrieves data',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                    'callback' => '_api_exbooth_get',
                    'access callback' => 'services_access_menu',
                    'access arguments' => array('view'),
                    'access arguments append' => TRUE,
                    'args' => array(
                        array(
                            'name' => 'key',
                            'type' => 'int',
                            'description' => 'The id',
                            'source' => array('path' => '0'),
                            'optional' => FALSE,
                        ),
                    ),
                ),
                'create' => array(
                    'help' => 'Creates a note',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                    'callback' => '_api_exbooth_actions',
                    'access arguments' => array('note resource create'),
                    'access arguments append' => FALSE,
                    'args' => array(
                        array(
                            'name' => 'data',
                            'type' => 'struct',
                            'description' => 'The note object',
                            'source' => 'data',
                            'optional' => FALSE,
                        ),
                    ),
                ),
                'update' => array(
                    'help' => 'Updates a note',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                    'callback' => '_api_exbooth_actions',
                    'access callback' => '_apiresource_access',
                    'access arguments' => array('update'),
                    'access arguments append' => TRUE,
                    'args' => array(
                        array(
                            'name' => 'id',
                            'type' => 'int',
                            'description' => 'The id of the node to update',
                            'source' => array('path' => '0'),
                            'optional' => FALSE,
                        ),
                        array(
                            'name' => 'data',
                            'type' => 'struct',
                            'description' => 'The note data object',
                            'source' => 'data',
                            'optional' => FALSE,
                        ),
                    ),
                ),
                'delete' => array(
                    'help' => 'Deletes a note',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                    'callback' => '_api_exbooth_actions',
                    'access callback' => '_apiresource_access',
                    'access arguments' => array('delete'),
                    'access arguments append' => TRUE,
                    'args' => array(
                        array(
                            'name' => 'nid',
                            'type' => 'int',
                            'description' => 'The id of the note to delete',
                            'source' => array('path' => '0'),
                            'optional' => FALSE,
                        ),
                    ),
                ),
                'index' => array(
                    'help' => 'Retrieves a listing',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                    'callback' => '_api_exbooth_actions',
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => FALSE,
                    'args' => array(array(
                            'name' => 'page',
                            'type' => 'int',
                            'description' => '',
                            'source' => array(
                                'param' => 'page',
                            ),
                            'optional' => TRUE,
                            'default value' => 0,
                        ),
                        array(
                            'name' => 'parameters',
                            'type' => 'array',
                            'description' => '',
                            'source' => 'param',
                            'optional' => TRUE,
                            'default value' => array(),
                        ),
                    ),
                ),
            ),
            'actions' => array(
                'password_reset' => array(
                    'help' => 'reset pass callback',
                    'callback' => '_api_password_reset',
                    'args' => array(
                        array(
                            'name' => 'resetCode',
                            'type' => 'string',
                            'description' => 'A valid reset code',
                            'source' => array('data' => 'resetCode'),
                            'optional' => FALSE,
                        ),
                        array(
                            'name' => 'password',
                            'type' => 'string',
                            'description' => 'A valid password',
                            'source' => array('data' => 'password'),
                            'optional' => FALSE,
                        ),
                    ),
                    'access callback' => 'services_access_menu',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                ),
                'searchProducts' => array(
                    'help' => 'search products',
                    'callback' => 'search_products',
                    'args' => array(
                        array(
                            'name' => 'serchQuery',
                            'type' => 'string',
                            'description' => 'A search query',
                            'source' => 'data',
                            'optional' => FALSE,
                        )
                    ),
                    'access callback' => 'services_access_menu',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                ),
                'searchUsers' => array(
                    'help' => 'search users',
                    'callback' => 'search_users',
                    'args' => array(
                        array(
                            'name' => 'serchQuery',
                            'type' => 'string',
                            'description' => 'A search query',
                            'source' => 'data',
                            'optional' => FALSE,
                        )
                    ),
                    'access callback' => 'services_access_menu',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                ),
                'searchHashtags' => array(
                    'help' => 'search Hashtags',
                    'callback' => 'search_hashtags',
                    'args' => array(
                        array(
                            'name' => 'serchQuery',
                            'type' => 'string',
                            'description' => 'A search query',
                            'source' => 'data',
                            'optional' => FALSE,
                        )
                    ),
                    'access callback' => 'services_access_menu',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                ),
                'instagram_user' => array(
                    'help' => 'add instagram user to loggedin user',
                    'callback' => '_instagram_user',
                    'args' => array(
                        array(
                            'name' => 'instagram',
                            'type' => 'array',
                            'description' => 'instagram data to tie with current logged in user',
                            'source' => 'data',
                            'optional' => FALSE,
                        )
                    ),
                    'access callback' => 'user_is_logged_in',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                ),
              'deleteMultipleComments' => array(
                    'help' => 'Delete multiple comments',
                    'callback' => 'delete_multiple_comments',
                    'args' => array(
                        array(
                            'name' => 'ids',
                            'type' => 'string',
                            'description' => 'Comment IDs',
                            'source' => 'data',
                            'optional' => FALSE,
                        )
                    ),
                    'access callback' => 'delete_multiple_comments_access',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                ),
              'notification_settings' => array(
                    'help' => 'Update notification settings',
                    'callback' => 'notification_settings',
                    'args' => array(
                        array(
                            'name' => 'status',
                            'type' => 'string',
                            'description' => '',
                            'source' => 'data',
                            'optional' => FALSE,
                        ),
                        array(
                            'name' => 'like_settings',
                            'type' => 'string',
                            'description' => '',
                            'source' => 'data',
                            'optional' => FALSE,
                        ),
                        array(
                            'name' => 'comment_settings',
                            'type' => 'string',
                            'description' => '',
                            'source' => 'data',
                            'optional' => FALSE,
                        ),
                        array(
                            'name' => 'device_type',
                            'type' => 'string',
                            'description' => '',
                            'source' => 'data',
                            'optional' => FALSE,
                        ),
                        array(
                            'name' => 'token',
                            'type' => 'string',
                            'description' => '',
                            'source' => 'data',
                            'optional' => FALSE,
                        )
                    ),
                    'access callback' => 'notification_settings_access',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                ),
                'updateToken' => array(
                    'help' => 'Update push notification token',
                    'callback' => 'push_notification_update_token',
                    'args' => array(
                        array(
                            'name' => 'device_type',
                            'type' => 'string',
                            'description' => '',
                            'source' => 'data',
                            'optional' => FALSE,
                        ),
                        array(
                            'name' => 'token',
                            'type' => 'string',
                            'description' => '',
                            'source' => 'data',
                            'optional' => FALSE,
                        )
                    ),
                    'access callback' => 'push_notification_update_token_access',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                ),
                'upload_item_images' => array(
                    'help' => 'Upload item images',
                    'callback' => 'upload_item_images',
                    'args' => array(
                        array(
                            'name' => 'status',
                            'type' => 'string',
                            'description' => '',
                            'source' => 'data',
                            'optional' => TRUE,
                        )
                    ),
                    'access callback' => 'upload_item_images_access',
                    'file' => array('type' => 'inc', 'module' => 'api', 'name' => 'resources/api_exbooth'),
                ),
            ),
        ),
    );
}
