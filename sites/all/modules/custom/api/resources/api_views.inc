<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * response for views returned in services 
 */
function api_views_structure($result, $args) {//echo json_encode($result); exit;

  switch ($args[0]['view_name']) {

    case 'product':
      $product = $result[0];
      /*$fields = 'cid,uid,name,subject,changed';
    
      $parameters = array('nid' => $product->pid, 'status' => '1');
      $product->comments = _comment_resource_index(0, $fields, $parameters, 100000);*/
      $_SESSION['is_api']=true;
      $product->comments = views_embed_view('comments','services_1',$product->pid);
        
      $product->exhibition_date = str_replace(' 00:00:00', '', $product->exhibition_date);
      if(strpos($product->exhibition_date,' to ')===false)
          $product->exhibition_date.=' to '.$product->exhibition_date; 
      if(empty($product->service_per))$product->service_per='';
      if(empty($product->author_avatar))$product->author_avatar='';
      if(empty($product->exhibition_date))$product->exhibition_date='';
      if(!empty($product->category))$product->category=html_entity_decode($product->category);
      
      if(!empty($product->price))
      {
        $price_arr = $product->price;
        $curr_info = currency_load($price_arr['currency_code']);
        $product->price = $price_arr['amount']/$curr_info->subunits;
        $product->currency = $price_arr['currency_code'];
      }
      return $product;
      break;
    case 'following':
      // Followers
      global $user;
      if($args[0]['display_id'] == 'services_2'){
          if(!empty($args[0]['args'][0]))
            $uid=$args[0]['args'][0];
          else
            $uid=$user->uid;
          
          $flag = flag_get_flag('follow_user');
          foreach($result['results'] as $one_res)
          {
            if(!empty($user->uid) && !empty($args[0]['args'][0]) && $args[0]['args'][0] != $user->uid)
            {
              $flagged = $flag->is_flagged($one_res->uid,$user->uid);
              if($flagged === true)
                $one_res->is_followed = '1';
              else
                $one_res->is_followed = '0';
              
              $flagged = $flag->is_flagged($user->uid,$one_res->uid);
              if($flagged === true)
                $one_res->is_following = '1';
              else
                $one_res->is_following = '0';
            }
            elseif(!empty($user->uid))
            {
              $one_res->is_following = '1';
              $flagged = $flag->is_flagged($one_res->uid,$user->uid);
              if($flagged === true)
                $one_res->is_followed = '1';
              else
                $one_res->is_followed = '0';
            }
            else
            {
                $one_res->is_followed = '0';
                $one_res->is_following = '0';
            }
          }
      }
      // Following
      elseif($args[0]['display_id'] == 'services_1'){
        global $user;
        $flag = flag_get_flag('follow_user');
        foreach($result['results'] as $one_res)
        {
          if(!empty($user->uid) && !empty($args[0]['args'][0]) && $args[0]['args'][0] != $user->uid)
          {
            $flagged = $flag->is_flagged($one_res->uid,$user->uid);
            if($flagged === true)
              $one_res->is_followed = '1';
            else
              $one_res->is_followed = '0';
            
            $flagged = $flag->is_flagged($user->uid,$one_res->uid);
            if($flagged === true)
              $one_res->is_following = '1';
            else
              $one_res->is_following = '0';
          }
          elseif(!empty($user->uid))
          {
            $one_res->is_followed = '1';
            $flagged = $flag->is_flagged($user->uid,$one_res->uid);
            if($flagged === true)
              $one_res->is_following = '1';
            else
              $one_res->is_following = '0';
          }
          else
          {
            $one_res->is_followed = '0';
            $one_res->is_following = '0';
          }
        }
      }
      return $result;
      break;
    case 'taxonomy_terms':
      if($args[0]['display_id'] == 'services_1'){
        foreach($result['results'] as $one_res){
          if(!empty($one_res->image[0]))
            $one_res->image=$one_res->image[0];
          else
            $one_res->image='';
        }
      }
      return $result;
      break;
    case 'products_api' :
      if($args[0]['display_id'] == 'services_5'){
        foreach($result['results'] as $one_product)
        {
          if(!empty($one_product->productPrice))
          {
            $price_arr = $one_product->productPrice;
            $curr_info = currency_load($price_arr['currency_code']);
            $one_product->productPrice = $price_arr['amount']/$curr_info->subunits;
            $one_product->currency = $price_arr['currency_code'];
          }
        }
      }
      elseif($args[0]['display_id'] == 'services_8'){
        global $user;
        $_SESSION['is_api']=true;
        $flag = flag_get_flag('like');
        foreach($result['results'] as $one_product)
        {
          $flagged = $flag->is_flagged($one_product->pid,$user->uid);
            if($flagged === true)
              $one_product->is_liked = '1';
            else
              $one_product->is_liked = '0';
          
          if(!empty($one_product->price))
          {
            $price_arr = $one_product->price;
            $curr_info = currency_load($price_arr['currency_code']);
            $one_product->price = $price_arr['amount']/$curr_info->subunits;
            $one_product->currency = $price_arr['currency_code'];
          }
          
          //$one_product->comments = views_embed_view('comments','services_1',$one_product->pid);
          $view = views_get_view('comments');
          $view->set_display('services_1');
          $view->set_arguments(array($one_product->pid));
          $view->pre_execute();
          $view->execute();
          $one_product->comments = $view->render();
          if($one_product->comments){
            foreach ($one_product->comments as $one_comment){
              $time_elapsed = time() - $one_comment->date;
              $seconds 	= $time_elapsed ;
              $minutes 	= floor($time_elapsed / 60 );
              $hours 		= floor($time_elapsed / 3600);
              $days 		= floor($time_elapsed / 86400 );
              $weeks 		= floor($time_elapsed / 604800);
              $years 		= floor($time_elapsed / 31207680 );

              if($years)$one_comment->date = $years.'y';
              elseif($weeks)$one_comment->date = $weeks.'w';
              elseif($days)$one_comment->date = $days.'d';
              elseif($hours)$one_comment->date = $hours.'h';
              elseif($minutes)$one_comment->date = $minutes.'m';
              elseif($seconds)$one_comment->date = $seconds.'s';

              if($one_comment->author_avatar){
                $file = file_load($one_comment->author_avatar);
                if(isset($file->uri))
                  $one_comment->author_avatar = str_replace($GLOBALS['base_root'],'',image_style_url('apithumbnail', $file->uri));
              }
            }
          }
          
          //$one_product->comments = $view->result;

          $one_product->exhibition_date = str_replace(' 00:00:00', '', $one_product->exhibition_date);
          if(strpos($one_product->exhibition_date,' to ')===false)
              $one_product->exhibition_date.=' to '.$one_product->exhibition_date; 
          if(empty($one_product->service_per))$one_product->service_per='';
          if(empty($one_product->author_avatar))$one_product->author_avatar='';
          if(empty($one_product->exhibition_date))$one_product->exhibition_date='';
          if(!empty($one_product->category))$one_product->category=html_entity_decode($one_product->category);
        }
      }
      return $result;
      break;
    default :
      return $result;
      break;
  }
}
