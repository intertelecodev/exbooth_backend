<?php
/**
 * @file
 * updates.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function updates_default_rules_configuration() {
  $items = array();
  $items['rules_content_expire'] = entity_import('rules_config', '{ "rules_content_expire" : {
      "LABEL" : "content expire",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "node_expire" ],
      "ON" : { "node_expired" : [] },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "node" ],
            "type" : "node",
            "bundle" : { "value" : { "product" : "product" } }
          }
        }
      ],
      "DO" : [ { "node_unpublish" : { "node" : [ "node" ] } } ]
    }
  }');
  $items['rules_remove_product_upon_node_delete'] = entity_import('rules_config', '{ "rules_remove_product_upon_node_delete" : {
      "LABEL" : "remove product upon node delete",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "1",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_delete--product" : { "bundle" : "product" } },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "product" : "product" } } } }
      ],
      "DO" : [
        { "entity_fetch" : {
            "USING" : {
              "type" : "commerce_product",
              "id" : [ "node:field-product:product-id" ]
            },
            "PROVIDE" : { "entity_fetched" : { "entity_fetched" : "Fetched entity" } }
          }
        },
        { "entity_delete" : { "data" : [ "entity-fetched" ] } }
      ]
    }
  }');
  return $items;
}
