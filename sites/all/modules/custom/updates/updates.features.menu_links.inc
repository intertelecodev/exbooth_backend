<?php
/**
 * @file
 * updates.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function updates_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_exhibition:node/add/product.
  $menu_links['navigation_exhibition:node/add/product'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/product',
    'router_path' => 'node/add/product',
    'link_title' => 'Exhibition',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'navigation_exhibition:node/add/product',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'navigation_add-content:node/add',
  );
  // Exported menu link: navigation_product:node/add/product/product.
  $menu_links['navigation_product:node/add/product/product'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/product/product',
    'router_path' => 'node/add/product',
    'link_title' => 'Product',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'navigation_product:node/add/product/product',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'navigation_add-content:node/add',
  );
  // Exported menu link: navigation_service:node/add/product/service.
  $menu_links['navigation_service:node/add/product/service'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/product/service',
    'router_path' => 'node/add/product',
    'link_title' => 'Service',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'navigation_service:node/add/product/service',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'navigation_add-content:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Exhibition');
  t('Product');
  t('Service');

  return $menu_links;
}
