<?php
/**
 * @file
 * updates.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function updates_commerce_product_default_types() {
  $items = array(
    'exhibition' => array(
      'type' => 'exhibition',
      'name' => 'Exhibition',
      'description' => '',
      'help' => '',
      'revision' => 1,
    ),
    'product' => array(
      'type' => 'product',
      'name' => 'Product',
      'description' => '',
      'help' => '',
      'revision' => 1,
    ),
    'service' => array(
      'type' => 'service',
      'name' => 'service',
      'description' => '',
      'help' => '',
      'revision' => 1,
    ),
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function updates_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_views_api().
 */
function updates_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function updates_flag_default_flags() {
  $flags = array();
  // Exported flag: "follow_user".
  $flags['follow_user'] = array(
    'entity_type' => 'user',
    'title' => 'follow_user',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Follow [user:name]',
    'flag_long' => '',
    'flag_message' => 'Following [user:name]',
    'unflag_short' => 'Unfollow [user:name]',
    'unflag_long' => '',
    'unflag_message' => 'Stopped following',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'token' => 0,
    ),
    'show_as_field' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'show_on_profile' => 0,
    'access_uid' => 'others',
    'module' => 'updates',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "inappropriate".
  $flags['inappropriate'] = array(
    'entity_type' => 'node',
    'title' => 'inappropriate',
    'global' => 0,
    'types' => array(
      0 => 'product',
    ),
    'flag_short' => 'Flag this item',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unflag this item',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'module' => 'updates',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "Like".
  $flags['like'] = array(
    'entity_type' => 'node',
    'title' => 'Like',
    'global' => 0,
    'types' => array(
      0 => 'product',
    ),
    'flag_short' => 'Like',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Dislike',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'module' => 'updates',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_image_default_styles().
 */
function updates_image_default_styles() {
  $styles = array();

  // Exported image style: 300x300.
  $styles['300x300'] = array(
    'label' => '300x300',
    'effects' => array(
      1 => array(
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 300,
          'height' => 300,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: 600x600.
  $styles['600x600'] = array(
    'label' => '600x600',
    'effects' => array(
      14 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 5000,
          'height' => 5000,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: 700x700.
  $styles['700x700'] = array(
    'label' => '700x700',
    'effects' => array(
      13 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 700,
          'height' => 700,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: apithumbnail.
  $styles['apithumbnail'] = array(
    'label' => 'apiThumbnail',
    'effects' => array(
      9 => array(
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 300,
          'height' => 300,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: cover_400x200.
  $styles['cover_400x200'] = array(
    'label' => 'cover 400x200',
    'effects' => array(
      12 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 600,
          'height' => 400,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function updates_node_info() {
  $items = array(
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_default_oauth2_server().
 */
function updates_default_oauth2_server() {
  $items = array();
  $items['main'] = entity_import('oauth2_server', '{
    "name" : "main",
    "label" : "main",
    "settings" : {
      "enforce_state" : true,
      "default_scope" : "scope1",
      "allow_implicit" : 0,
      "use_openid_connect" : 0,
      "use_crypto_tokens" : 0,
      "grant_types" : {
        "client_credentials" : "client_credentials",
        "authorization_code" : 0,
        "urn:ietf:params:oauth:grant-type:jwt-bearer" : 0,
        "refresh_token" : 0,
        "password" : 0
      },
      "always_issue_new_refresh_token" : 1,
      "access_lifetime" : "3600",
      "id_lifetime" : "3600",
      "refresh_token_lifetime" : "1209600",
      "require_exact_redirect_uri" : 0
    },
    "scopes" : [
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" },
      { "name" : "scope1", "description" : "scope1" }
    ]
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_index().
 */
function updates_default_search_api_index() {
  $items = array();
  $items['hashtags_taxonomy_search'] = entity_import('search_api_index', '{
    "name" : "Hashtags taxonomy search",
    "machine_name" : "hashtags_taxonomy_search",
    "description" : null,
    "server" : "api",
    "item_type" : "taxonomy_term",
    "options" : {
      "datasource" : { "bundles" : [] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "name" : { "type" : "text" },
        "node_count" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_ranges_alter" : { "status" : 0, "weight" : "-50", "settings" : [] },
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "default" : "0", "bundles" : { "hashtags" : "hashtags" } }
        },
        "search_api_alter_language_control" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "name" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "name" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "name" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "name" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  $items['user'] = entity_import('search_api_index', '{
    "name" : "user",
    "machine_name" : "user",
    "description" : null,
    "server" : "api",
    "item_type" : "user",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "field_country" : { "type" : "string", "entity_type" : "country" },
        "name" : { "type" : "text" },
        "search_api_language" : { "type" : "string" },
        "uid" : { "type" : "integer" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_role_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "default" : "0", "roles" : { "2" : "2" } }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : { "name" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "name" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "name" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "name" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function updates_default_search_api_server() {
  $items = array();
  $items['api'] = entity_import('search_api_server', '{
    "name" : "api",
    "machine_name" : "api",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : "1",
      "partial_matches" : 1,
      "indexes" : {
        "product_display" : {
          "search_api_language" : {
            "table" : "search_api_db_product_display",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "nid" : {
            "table" : "search_api_db_product_display",
            "column" : "nid_1",
            "type" : "integer",
            "boost" : "1.0"
          },
          "type" : {
            "table" : "search_api_db_product_display",
            "column" : "type_1",
            "type" : "string",
            "boost" : "1.0"
          },
          "title" : {
            "table" : "search_api_db_product_display",
            "column" : "title_1",
            "type" : "string",
            "boost" : "1.0"
          },
          "status" : {
            "table" : "search_api_db_product_display",
            "column" : "status_1",
            "type" : "integer",
            "boost" : "1.0"
          },
          "created" : {
            "table" : "search_api_db_product_display",
            "column" : "created_1",
            "type" : "date",
            "boost" : "1.0"
          },
          "changed" : {
            "table" : "search_api_db_product_display",
            "column" : "changed_1",
            "type" : "date",
            "boost" : "1.0"
          },
          "field_product" : {
            "table" : "search_api_db_product_display",
            "column" : "field_product_1",
            "type" : "integer",
            "boost" : "1.0"
          },
          "search_api_aggregation_1" : {
            "table" : "search_api_db_product_display_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_product:field_caption:value" : {
            "table" : "search_api_db_product_display",
            "type" : "string",
            "boost" : "1.0",
            "column" : "field_product_field_caption_value"
          },
          "field_product:commerce_price:amount" : {
            "table" : "search_api_db_product_display",
            "column" : "field_product_commerce_price_amount",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_vote:average_rating" : {
            "table" : "search_api_db_product_display",
            "column" : "field_vote_average_rating",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_vote:rating_count" : {
            "table" : "search_api_db_product_display",
            "column" : "field_vote_rating_count",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product:field_category" : {
            "table" : "search_api_db_product_display",
            "column" : "field_product_field_category",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product:field_country" : {
            "table" : "search_api_db_product_display",
            "column" : "field_product_field_country",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_product:type" : {
            "table" : "search_api_db_product_display",
            "column" : "field_product_type",
            "type" : "string",
            "boost" : "1.0"
          }
        },
        "user" : {
          "uid" : {
            "table" : "search_api_db_user",
            "column" : "uid",
            "type" : "integer",
            "boost" : "1.0"
          },
          "name" : { "table" : "search_api_db_user_text", "type" : "text", "boost" : "1.0" },
          "search_api_language" : {
            "table" : "search_api_db_user",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_country" : {
            "table" : "search_api_db_user",
            "column" : "field_country",
            "type" : "string",
            "boost" : "1.0"
          }
        },
        "hashtags_taxonomy_search" : {
          "name" : {
            "table" : "search_api_db_hashtags_taxonomy_search_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "search_api_language" : {
            "table" : "search_api_db_hashtags_taxonomy_search",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "node_count" : {
            "table" : "search_api_db_hashtags_taxonomy_search",
            "column" : "node_count",
            "type" : "integer",
            "boost" : "1.0"
          }
        }
      }
    },
    "enabled" : "1"
  }');
  return $items;
}
