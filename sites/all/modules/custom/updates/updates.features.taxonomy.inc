<?php
/**
 * @file
 * updates.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function updates_taxonomy_default_vocabularies() {
  return array(
    'exhibition_category' => array(
      'name' => 'Exhibition category',
      'machine_name' => 'exhibition_category',
      'description' => 'Exhibition category',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 1,
    ),
    'hashtags' => array(
      'name' => 'Hashtags',
      'machine_name' => 'hashtags',
      'description' => 'Hashtag vocabulary',
      'hierarchy' => 0,
      'module' => 'hashtags',
      'weight' => -11,
      'language' => 'und',
      'i18n_mode' => 0,
    ),
    'product_category' => array(
      'name' => 'product category',
      'machine_name' => 'product_category',
      'description' => 'product category',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 1,
    ),
    'service_category' => array(
      'name' => 'Service category',
      'machine_name' => 'service_category',
      'description' => 'Service category',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 1,
    ),
    'services_per_options' => array(
      'name' => 'services per options',
      'machine_name' => 'services_per_options',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 1,
    ),
  );
}
